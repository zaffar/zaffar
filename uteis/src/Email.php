<?php
/**
 * Validad e o dominio do email é real e acerta o email do usuario.
 *
 *
 * @package  Zaffar
 * @author   Mauricio Zaffarani Rezende <zaffar@zaffar.com.br>
 * @version  v0.1
 * @access   public
 * @see      http://zaffar.com.br/zaffar
 */

namespace Zaffar\Uteis;

/**
 * Exemplo de uso:
 * $e = new Email();
 * $e->valida('email@teste.com.br');
 * $e->acertar('email@teste.com.br');
 */
class Email {

	/**
     * Verifica se o domínio do email é válido.
	 *
     * @param string $email Email a ser testado
     * @access public
     * @return boolean
     */
	public function valida($email){
		$exp = "/^[a-z\'0-9]+([._-][a-z\'0-9]+)*@([a-z0-9]+([._-][a-z0-9]+))+$/i";
		$emailArray = explode("@",$email);
		$dominio = array_pop($emailArray);
		if(preg_match($exp,$email)){
			if(checkdnsrr($dominio,"MX")){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}   
	}

	/**
	 * Acerta o email colocando todos os nme em minúsculas.
	 *
	 * @param string $email Email a ser arrumado
	 * @access public
	 * @return string
	 */
	public function acertar($email){
		return trim(strtolower($email));
	}
}