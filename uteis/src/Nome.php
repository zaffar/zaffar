<?php
/**
 * Trabalha com nomes próprios.
 *
 *
 * @package  Zaffar
 * @author   Mauricio Zaffarani Rezende <zaffar@zaffar.com.br>
 * @version  v0.1
 * @access   public
 * @see      http://zaffar.com.br/zaffar
 */
namespace Zaffar\Uteis;

/**
 * Exemplo de uso:
 * $n = new Nome();
 * $n->acertar('NOME DA Pessoa'); retorna Nome Da Pessoa
 * $n->resumo('NOME DA Pessoa'); retorna Nome Pessoa
 * $n->encurtar('NOME DA Pessoa', 1); retorna Nome
 */
final class Nome {


    /**
     * Acerta o nome colocando todos os caracteres em minúsculas
     * e com as primeiras letras em maiúscula.
     *
     * @param string $nome Nome a ser arrumado
     * @access public
     * @return string
     */
    public function acertar($nome){
        $saida = '';
        $nome = explode(' ',trim(strip_tags(strtolower($nome))));

        foreach ($nome as $key => $t) {

            if(strlen($t) <= 2)
                $saida .= $t . ' ';
            else
                $saida .= ucfirst($t) . ' ';
        }

        return trim($saida);
    }

    /**
     * Retorna o primeiro e o último nome.
     *
     * @param string $nome Nome da pessoa
     * @access public
     * @return string
     */
    public function resumo($nome){
        $nome = explode(' ',trim($nome));

        if(count($nome)>1)
            $nomeFinal = trim(ucfirst(strtolower($nome[0])) ." ". ucfirst(strtolower($nome[count($nome)-1])));
        else
            $nomeFinal = trim(ucfirst(strtolower($nome[0])));

        return $this->acertar($nomeFinal);
    }

    /**
     * Retorna a quatidade de palavras de um nome.
     *
     * @param string $nome Nome da pessoa
     * @param integer $tamanho Quatidade de nomes a serem retornados
     * @access public
     * @return string
     */
    public function encurtar($nome, $tamanho=1){
        $nome = explode(' ',trim($nome));
        $nomeFinal = '';
        for($i=0;$i<$tamanho;$i++)
            $nomeFinal .= $nome[$i].' ';

        return $this->acertar($nomeFinal);
    }
}
?>