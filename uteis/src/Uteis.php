<?php
/**
 * Diversos methodos uteis em geral.
 *
 *
 * @package  Zaffar
 * @author   Mauricio Zaffarani Rezende <zaffar@zaffar.com.br>
 * @version  v0.1
 * @access   public
 * @see      http://zaffar.com.br/zaffar
 */
namespace Zaffar\Uteis;

/**
 * Exemplo de uso:
 * $u = new Uteis();
 * $u->redirecionar('http://exemplo.com.br');
 * $u->slugfy('João Paulo?'); retorna joao-paulo
 */
final class Uteis  {
	
	/**
     * Redireciona o navegador para a URL informada. Independete de já foi dado alguma saida antes.
	 *
     * @param string $url URL de destino
     * @access public
     */
	public function redirecionar($url){
		exit('<META HTTP-EQUIV="Refresh" Content="0; URL='.$url.'">');
		die();
	}

	/**
     * Retorna uma string sem acentos ou caracteres especiais
     * e troca estaços por -
	 *
     * @param string $string String a ser reduzida
     * @access public
     * @return string
     */
	public function slugfy($string){
		return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'))), ' '));
	}
	
	/**
     * Aplica uma amscara a uma string.
     * ex. Entrando uma string 0123456789 com uma máscara
     * (##) ####-####, retorna o valor (01) 2345-6789
	 *
     * @param string $string String a ser aplicada a máscara
     * @param string $mascara Modelo da mascara a ser aplicada
     * @access public
     * @return string
     */
	public function mascara($string, $mascara){
		$maskared = '';
		$k = 0;
		for($i = 0; $i<=strlen($mascara)-1; $i++){
			if($mascara[$i] == '#'){
				if(isset($string[$k]))
					$maskared .= $string[$k++];
			}else{
				if(isset($mascara[$i]))
					$maskared .= $mascara[$i];
			}
		}
		return $maskared;
	}
	
	/**
     * Encurta um texto com a quantidade de caracteres estipulado
     * removendo as tags html e encerrando na palavra inteira anterior
     * ao numero estipulado, caso a string seja maior adiciona ...
     * no final da string.
	 *
     * @param string $texto Texto a ser encurtado
     * @param string $quantidade Quatidade de caracteres a ser retornado.
     * @access public
     * @return string
     */
	public function encurtar($texto, $quantidade){
		$texto = strip_tags($texto);
		if(strlen($texto) < $quantidade){
			return $texto;
		}else{
			
			$subtexto = trim(substr($texto,0,$quantidade));
			$subtextoExplode = explode(" ",$subtexto);
			
			$textoFinal = "";
			for($i=0; $i < count($subtextoExplode)-1; $i++)
				$textoFinal .= $subtextoExplode[$i] . " ";
					
			return trim($textoFinal) . "...";
		}
	}

	/**
     * Retorna uma string com a senha gerada.
	 *
     * @param integer $length Tamanho da senha
     * @param boolean $add_dashes Adicioba traços
     * @param string $available_sets Caracteres na senha 'luds' (Lowercase, Uppercase, Digits, Spaeicla Caracters)
     * @param string $mensagem Mensagem do email (HTML)
     * @return string
     * @access public
     */
	public function gerarSenha($length = 8, $add_dashes = false, $available_sets = 'luds') {
		$sets = array();
		if(strpos($available_sets, 'l') !== false)
			$sets[] = 'abcdefghjkmnpqrstuvwxyz';
		if(strpos($available_sets, 'u') !== false)
			$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
		if(strpos($available_sets, 'd') !== false)
			$sets[] = '23456789';
		if(strpos($available_sets, 's') !== false)
			$sets[] = '!@#$%&*?';

		$all = '';
		$password = '';
		foreach($sets as $set)
		{
			$password .= $set[array_rand(str_split($set))];
			$all .= $set;
		}

		$all = str_split($all);
		for($i = 0; $i < $length - count($sets); $i++)
			$password .= $all[array_rand($all)];

		$password = str_shuffle($password);

		if(!$add_dashes)
			return $password;

		$dash_len = floor(sqrt($length));
		$dash_str = '';
		while(strlen($password) > $dash_len)
		{
			$dash_str .= substr($password, 0, $dash_len) . '-';
			$password = substr($password, $dash_len);
		}
		$dash_str .= $password;
		return $dash_str;
	}

	/**
     * Retorna IP do cliente.
	 *
     * @return string
     * @access public
     */
	public function getIp() {
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'DESCONHECIDO';
	    return $ipaddress;
	}

	/**
	 * Verifica se uma data é válida
	 *
	 * @param string $data Data a ser validada
	 * @param string $formato Formato que a data é enviada.
	 * @access public
	 * @return string
	 */
	public function validaData($data, $formato = 'Y-m-d H:i:s'){
		$d = DateTime::createFromFormat($formato, $data);
		return $d && $d->format($formato) == $data;
	}
}
?>