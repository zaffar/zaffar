<?php
/**
 * Trabalha com valores monetário.
 *
 *
 * @package  Zaffar
 * @author   Mauricio Zaffarani Rezende <zaffar@zaffar.com.br>
 * @version  v0.1
 * @access   public
 * @see      http://zaffar.com.br/zaffar
 */

namespace Zaffar\Uteis;

/**
 * Exemplo de uso:
 * $m = new Monetario();
 * $m->valorBrasil('222.00');
 * $m->valorPorExtenso('222.00', true, true);
 */
final class Monetario  {
	
	/**
     * Retorna o valor em Reais com o R$.
	 *
     * @param double $valor Valor a ser retornado
     * @access public
     * @return string
     */
	public function valorBrasil($valor){
		$valor = str_replace(",", ".", $valor);
		return "R$ ".number_format($valor,2,',', '.');
	}

	/**
	 * Retorna o valor em Float.
	 *
	 * @param float $valor Valor a ser retornado
	 * @access public
	 * @return string
	 */
	public function valor2Float($valor){
		$valor = str_replace(".", "", $valor);
		$valor = str_replace(",", ".", $valor);
		return $valor;
	}

	/**
     * Retorna valor por extenço.
	 *
     * @param double $valor Valor a ser retornado pro extenço
     * @param boolean $maiusculas Retorna promeiras letras em maiúsculas
     * @param boolean $moeda Adiciona unidade da moeda
     * @return string
     * @access public
     */
	function valorPorExtenso($valor = 0, $maiusculas = false, $moeda=true) { 
		if($moeda){
			$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
			$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
		}else{
			$singular = array("", "", "", "", "", "", "");
			$plural = array("", "", "", "", "", "","");
		}
		
		$c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"); 
		$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"); 
		$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"); 
		$u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove"); 
		
		$z = 0; 
		$rt = "";
		
		$valor = number_format($valor, 2, ".", "."); 
		$inteiro = explode(".", $valor); 
		for($i=0;$i<count($inteiro);$i++) 
		for($ii=strlen($inteiro[$i]);$ii<3;$ii++) 
		$inteiro[$i] = "0".$inteiro[$i]; 
		
		$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2); 
		for ($i=0;$i<count($inteiro);$i++) { 
		$valor = $inteiro[$i]; 
		$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]]; 
		$rd = ($valor[1] < 2) ? "" : $d[$valor[1]]; 
		$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : ""; 
		
		$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && 
		$ru) ? " e " : "").$ru; 
		$t = count($inteiro)-1-$i; 
		$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : ""; 
		if ($valor == "000")$z++; elseif ($z > 0) $z--; 
		if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t]; 
		if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && 
		($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r; 
		} 
		
		if(!$maiusculas){ 
		return($rt ? $rt : "zero"); 
		} else { 
		
		if ($rt) $rt=ereg_replace(" E "," e ",ucwords($rt));
		return (($rt) ? ($rt) : "Zero"); 
		} 
	}
}
?>