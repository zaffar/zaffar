<?php
/**
 * Cria sessão segura, faz controle de login, chega se usuario esta logado e faz validação checkbrute
 * para validar se esta tendo muita tentativa de um usuario e bloqueia este.
 *
 *
 * @package  Zaffar
 * @author   Mauricio Zaffarani Rezende <zaffar@zaffar.com.br>
 * @version  v0.1
 * @access   public
 * @see      http://zaffar.com.br/zaffar
 */

namespace Zaffar\Seguranca;
use Zaffar\Banco\PDOAdapter;

	/**
	 * Exemplo de uso:
	 * $seguranca = new Seguranca();
	 * $seguranca->sec_session_start();
	 * $seguranca->login('usuario', 'senha', true);
	 * $seguranca->login_check();
	 */
class Seguranca  {
	
	/**
     * Tabela que esta os dados de login dos usuários.
     * @type String
     */
	private $tabela;

	/**
     * Campo esta o login do usuário.
     * @type String
     */
	private $campo_login;

	/**
     * Objeto de conexão com o banco.
     * @type PDOAdapter
     */
	private $db;

	/**
     * Seta tabela, campo_login e seta o db com PDOAdapter.
	 *
     * @param String $tabela Tabela que esta os dados de login dos usuários
     * @param String $campo_login Campo esta o login do usuário
	 * @param Array $dados_banco Dados de conexão com o banco (nome, usuario, senha, ip)
     * @access public
     */
	public function __construct($tabela, $campo_login, $dados_banco){
		$this->tabela = $tabela;
		$this->campo_login = $campo_login;
		$this->db = new PDOAdapter($dados_banco['name'],$dados_banco['user'],$dados_banco['password'],$dados_banco['host']);
	}
	
	/**
     * Inicia Sessão segura.
	 *
     * @access public
     * @return stdClass
     */
	public function sec_session_start() {
	    $session_name = 'sec_session';
	    $secure = true;
	    $httponly = true;

	    // if (ini_set('session.use_only_cookies', 1) === FALSE) {
	    //     header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
	    //     exit();
	    // }

	    // $cookieParams = session_get_cookie_params();
	    // session_set_cookie_params($cookieParams["lifetime"],
	    //     $cookieParams["path"], 
	    //     $cookieParams["domain"], 
	    //     $secure,
	    //     $httponly);

	    session_name($session_name);
	    session_start();
	    session_regenerate_id();
	}

	/**
     * Faz o login no sistema.
	 *
     * @param string $login Login do usuário
     * @param string $senha Senha do usuário
     * @param boolean $checkBrute Verifica ou não o CheckBrute
     * @access public
     * @return boolean
     */
	public function login($login, $senha, $checkBrute) {

		$sql = "SELECT * FROM " . $this->tabela . " WHERE " . $this->campo_login . "=:login AND ativo=1 LIMIT 1";
		$dadosBind = array('login' => $login);
		$login = $this->db->query($sql,$dadosBind);
		
		$salt = ( isset($login[0]['salt']))?$login[0]['salt']:'';

		$senha = hash('sha512', $senha . $salt);
		if(count($login) == 1){

			$id = ( isset($login[0]['id']))?$login[0]['id']:'';
			$senha_banco = (isset($login[0]['senha']))?$login[0]['senha']:'';


	        if ($this->checkbrute($id) == true) {

	            $sql = "UPDATE " . $this->tabela . " SET ativo=0 WHERE id=:id";
				$dadosBind = array('id' => $id);
				$this->db->ubQuery($sql,$dadosBind);
	            return false;

	        } else {

	            if ($senha_banco == $senha) {
	                $user_browser = $_SERVER['HTTP_USER_AGENT'];
	                $id = preg_replace("/[^0-9]+/", "", $id);
	                $_SESSION['id'] = $id;
	                $email = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $login[0]['email']);
	                $_SESSION['email'] = $email;
	                $_SESSION['login_string'] = hash('sha512', $senha . $user_browser);
	                return true;

	            } else if($checkBrute) {

	                $sql = "INSERT INTO tentativas_login (usuario, tempo) VALUES (:usuario, :tempo)";
					$dadosBind = array(
						'usuario' => $id,
						'tempo' => time()
					);
					$this->db->ubQuery($sql,$dadosBind);
	                return false;
	            }
	        }
	    } else {
	        return false;
	    }
	} 

	/**
     * Verifica se o usuário fez muitas tentativas de acesso
     * erradas e bloqueia o acesso caso afirmativo.
	 *
     * @param string $usuario Id do usuário
     * @access public
     * @return boolean
     */
	public function checkbrute($usuario) {

	    $now = time();
	    $valid_attempts = $now - (2 * 60 * 60);
	 	
	 	$sql = "SELECT tempo FROM tentativas_login WHERE usuario=:usuario AND tempo>=:valid_attempts";
		$dadosBind = array(
			'usuario' => $usuario,
			'valid_attempts' => $valid_attempts
		);
		$checaTentativas = $this->db->query($sql,$dadosBind);
		
	    if ($checaTentativas) {
	        if (count($checaTentativas) > 5) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	}

	/**
     * Verifica se o usuário está logado no sistema.
	 *
     * @access public
     * @return boolean
     */
	function login_check() {
	    if (isset($_SESSION['id'], $_SESSION['email'], $_SESSION['login_string'])) {
	 
	        $id = $_SESSION['id'];
	        $login_string = $_SESSION['login_string'];
	        $email = $_SESSION['email'];
	 
	        $user_browser = $_SERVER['HTTP_USER_AGENT'];
	 		

	 		$sql = "SELECT senha FROM " . $this->tabela . " WHERE id=:id LIMIT 1";
			$dadosBind = array('id' => $id);

	        if ( $checaLogin = $this->db->query($sql,$dadosBind) ) {
	            if(count($checaLogin) == 1){
	                $senha = (isset($checaLogin[0]['senha']))?$checaLogin[0]['senha']:'';
	                $login_check = hash('sha512', $senha . $user_browser);
	 
	                if ($login_check == $login_string) {
	                    return true;
	                } else {
	                    return false;
	                }
	            } else {
	                return false;
	            }
	        } else {
	            return false;
	        }
	    } else {
	        return false;
	    }
	}
}
?>