<?php
/**
 * Faz transações com o banco de dados via PDO.
 *
 *
 * Exemplo de uso:
 * $db = new PDOAdapter('nome_do_banco','usuario_do_banco','senha_do_banco','ip_do_banco');
 * $lista = $db->query('SELECT * FROM tabela WHERE id=:id, array('id'=>66));
 * $edita = $db->ubQuery('UPDATE tabela SET nome=:nome WHERE id=:id, array('nome'=>'Teste','id'=>66));
 *
 *
 * @package  Zaffar
 * @author   Mauricio Zaffarani Rezende <zaffar@zaffar.com.br>
 * @version  v0.1
 * @access   public
 * @see      http://zaffar.com.br/zaffar
 */

namespace Zaffar\Banco;
use \PDO;

/**
 * Exemplo de uso:
 * $db = new PDOAdapter('nome_do_banco','usuario_do_banco','senha_do_banco','ip_do_banco');
 * $lista = $db->query('SELECT * FROM tabela WHERE id=:id, array('id'=>66));
 * $edita = $db->ubQuery('UPDATE tabela SET nome=:nome WHERE id=:id, array('nome'=>'Teste','id'=>66));
 */
class PDOAdapter {

	/**
	* PDO object.
	 *
	* @var object
	*/
	private $db;

	/**
	 * Nome do banco.
	 *
	 * @var $name
	 * @type String
	 */
	private $name;

	/**
	 * Usuário do banco.
	 *
	 * @var $user
	 * @type String
	 */
	private $user;

	/**
	 * Senha do banco.
	 *
	 * @var $password
	 * @type String
	 */
	private $password;

	/**
	 * Ip do banco.
	 *
	 * @var $host
	 * @type String
	 */
	private $host;

	/**
	 * Instancia a classe e seta dados para a conexão do banco.
	 *
	 * @access public
	 * @param String $name Usuário do banco.
	 * @param String $user Usuário do banco.
	 * @param String $password Senha do banco.
	 * @param String $host Ip do banco.
	 */
	public function __construct($name, $user, $password, $host){
		$this->name 	= $name;
		$this->user 	= $user;
		$this->password = $password;
		$this->host 	= $host;
	}

	/**
	 * Conecta ao banco.
	 *
	 * @access private
	 * @return Boolean
	 */
	private function connect(){
		try {
			$this->db = new PDO('mysql:host='.$this->host.';dbName='.$this->name, $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'));
			$this->db->exec('USE '.$this->name.';');
			
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
			return true;
		} catch(PDOException $e){
			$this->handleError( $e );
			return false;
		}
	}

	/**
	 * Usar para retornar lista de dados. Usado em SELECT.
	 *
	 * @param string $query SQL query
	 * @param array $binds Dados
	 * @return array
	 */
	public function query($query, array $binds =array()){
		if ( is_null($this->db) )
			$this->connect();
		
		try {
			$stmt = $this->db->prepare($query);
			$stmt->execute($binds);
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
			$this->db;
		} catch(PDOException $e){
			$this->handleError( $e );
			return false;
		}
	} 

	/**
	 * Usar quando não se espera uma lista de dados. Usado em INSERT, UPDATE, DELETE.
	 *
	 * @param string $query SQL query
	 * @param array $binds Dados
	 * @return boolean TRUE quando valido, FALSE quando der erro
	 */
	public function ubQuery($query, array $binds = array()){
		if ( is_null($this->db) ){
			$this->connect();
		}
		
		try {
			$stmt = $this->db->prepare($query);
			$stmt->execute($binds);
			$this->db;
			return true;
		} catch(PDOException $e){
			$this->handleError( $e );
			return false;// - in production
		}
	}
	
	/**
	 * Manipula os erros gerados no PDO
	 * @param Exception $e Erro capturado
	 */
	private function handleError($e){
		echo '<b>Erro:</b> '.nl2br($e->getMessage()).$e;
	}

	/**
	 * Fecha conexão com o banco.
	 */
	public function close(){
		$this->db = null;
	}
}
?>