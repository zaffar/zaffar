<?php
/**
 * Backup cria backup de um banco de dados inteiro ou apenas de uma tabela especifica.
 *
 *
 * Exemplo de uso:
 * $dados_do_banco = array(
 *      'name' => 'nome_do_banco',
 *      'user' => 'usuario_do_banco',
 *      'password' => 'senha_do_banco',
 *      'host' => 'ip_do_banco',
 * );
 * $bkp = new Backup('/nome_da_pasta/', $dados_do_banco);
 *
 *
 * @package  Zaffar
 * @author   Mauricio Zaffarani Rezende <zaffar@zaffar.com.br>
 * @version  v0.1
 * @access   public
 * @see      http://zaffar.com.br/zaffar
 */

namespace Zaffar\Banco;
use Zaffar\Banco\PDOAdapter;

/**
 * Exemplo de uso:
 * $dados_do_banco = array(
 *      'name' => 'nome_do_banco',
 *      'user' => 'usuario_do_banco',
 *      'password' => 'senha_do_banco',
 *      'host' => 'ip_do_banco',
 * );
 * $bkp = new Backup('/nome_da_pasta/', $dados_do_banco);
 */
class Backup
{
    /**
     * Objeto de conexão com o banco.
     *
     * @var $db
     * @type PDOAdapter
     */
    private $db;

    /**
     * Pasta que será salvo o backup.
     *
     * @var $pasta
     * @type String
     */
    private $pasta;

    /**
     * Espesifica tabela para faze ro backup, deixe nulo para fazer de todas.
     *
     * @var $tabela
     * @type String
     */
    private $tabela;

    /**
     * Instancia a classe backup recebendo a pasta que será salvo o .sql,
     * os dadfos de conexão com o banco e a tabela a ser salva, ou nulo
     * para salvar todas as tabelas
     *
     * @param String $pasta Pasta que será salvo o backup.
     * @param Array $dados_banco dados para a conexão do banco (nome, usuario, senha, e ip)
     * @param String $tabela Tabela a ser salva, ou nulo para salvar todas
     */
    public function __construct($pasta, $dados_banco, $tabela = null){
        if(file_exists($pasta)){
            $this->pasta = $pasta;
            $this->tabela = $tabela;
            $this->db = new PDOAdapter($dados_banco['name'],$dados_banco['user'],$dados_banco['password'],$dados_banco['host']);
            $this->backup_tables();
        }else{
            throw new Exception('A pasta ' . $pasta . ' não existe.');
        }
    }

    /**
     * Cria backup do banco.
     *
     * @access private
     */
    private function backup_tables() {

        if($this->tabela == null)
            $tabelas = $this->db->query("SHOW FULL TABLES WHERE Table_Type = 'BASE TABLE'");
        else
            $tabelas = $this->db->query("SHOW FULL TABLES WHERE Table_Type = 'BASE TABLE' AND Tables_in_marketing = '".$this->tabela."'");
        $return = '';

        foreach($tabelas as $t) {
            $dados = $this->db->query('SELECT * FROM '.$t['Tables_in_marketing']);

            $return.= 'DROP TABLE '.$t['Tables_in_marketing'].';';
            $row2 = $this->db->query('SHOW CREATE TABLE '.$t['Tables_in_marketing']);
            $return.= "\n\n".$row2[0]['Create Table'].";\n\n";

            foreach ($dados as $dado) {
                $return.= 'INSERT INTO '.$t['Tables_in_marketing'].' VALUES(';

                $dado_qtd = count($dado);
                $passagem = 0;
                foreach ($dado as $d) {
                    $d = addslashes($d);
                    $d = preg_replace('/\n/','/\\n/',$d);
                    if (isset($d)) { $return.= '"'.$d.'"' ; } else { $return.= '""'; }
                    if ($passagem<($dado_qtd-1)) { $return.= ','; }
                    $passagem++;
                }
                $return.= ");\n";
            }
            $return.="\n\n\n";
        }

        $handle = fopen($this->pasta . DIRECTORY_SEPARATOR . date("Y-m-d").'.sql','w+');
        fwrite($handle,$return);
        fclose($handle);
    }
}