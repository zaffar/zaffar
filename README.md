# README #

Arquivos úteis para pequenos projetos

### Para que serve ###

* Trabalhar com banco via PDO
* Fazer backups de bancos MYSQL
* Fazer login com force brute e valida usuario logado
* Validar DSN de email 
* Trabalhar com valores monetários
* Formatar nomes próprios
* Valida CNPJ / CPF
* Entre outros métodos úteis (encurtar texto, slugfy, redirecionar) 

### Instalação ###

Via composer

```
#!Javascript

{
  "repositories": [
    {"type": "git", "url": "https://zaffar@bitbucket.org/zaffar/zaffar.git"}
  ]
,
  "require": {
    "php": ">=5.3.3",
    "zaffar/zaffar": "*"
  },
  "minimum-stability": "dev"
}
```